using DG.Tweening;
using Qte;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeatIndicatorScript : MonoBehaviour
{

    [SerializeField] private QteData data;
    [SerializeField] private Image beatBar;
    [SerializeField] private Image leftStart;
    [SerializeField] private Image rightStart;
    [SerializeField] private Image middle;
    [SerializeField] private Image leftBar;
    [SerializeField] private Image rightBar;

    private List<Image> _excessBars = new();
    private float _travelTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        _travelTime = 60f / (float)data.BeatPerMinute;

        data.OnTick += HandleOnTick;
    }

    private void HandleOnTick()
    {
        leftBar.transform.DOKill();
        rightBar.transform.DOKill();

        leftBar.transform.position = leftStart.transform.position;
        rightBar.transform.position= rightStart.transform.position;

        leftBar.transform.DOMove(middle.transform.position, _travelTime);
        rightBar.transform.DOMove(middle.transform.position, _travelTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
