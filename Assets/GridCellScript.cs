using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCellScript : MonoBehaviour
{
    [SerializeField] private bool isPlayerDamage;
    [SerializeField] private GridCellScript nextCell;
    [SerializeField] private GridCellScript prevCell;

    public GridCellScript GetNextCell(bool isUpward)
    {
        return isUpward ? prevCell : nextCell;
    }
}
