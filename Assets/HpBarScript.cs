using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpBarScript : MonoBehaviour
{
    public Action OnPlayerZeroHp;

    [SerializeField] private int maxHp = 10;
    [SerializeField] private Image hpFill;

    private int _currentHp;

    public void DealDamage(int dmg)
    {
        _currentHp = Math.Clamp(_currentHp - dmg, 0, maxHp);
        var fillFraction = (float)_currentHp / (float)maxHp;
        hpFill.DOFillAmount(fillFraction, 0.25f);
        if (_currentHp == 0)
        {
            //OnPlayerZeroHp.Invoke();
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        _currentHp = maxHp;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
