//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.7.0
//     from Assets/Input/PlayerInput.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public partial class @PlayerInput: IInputActionCollection2, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInput"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""2c1d5274-1ee6-46a4-a677-30da0829cd27"",
            ""actions"": [
                {
                    ""name"": ""QteA"",
                    ""type"": ""Button"",
                    ""id"": ""9767f004-5d98-4a9c-9a16-d69603678f95"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""QteB"",
                    ""type"": ""Button"",
                    ""id"": ""bf17a9eb-c5c6-45e2-86f6-8c3498a6ea35"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""QteX"",
                    ""type"": ""Button"",
                    ""id"": ""8154a2fa-acf6-4fe0-b4e5-fcbc4b68445f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Tap"",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""QteY"",
                    ""type"": ""Button"",
                    ""id"": ""d569aacb-3db2-4b84-b341-78da9d0f882d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Layer1"",
                    ""type"": ""Button"",
                    ""id"": ""32f98810-db32-44f6-8a01-9a8a05d2d7a0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Layer2"",
                    ""type"": ""Button"",
                    ""id"": ""82e1f15b-ac58-4f08-b01f-eae80639a495"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Layer3"",
                    ""type"": ""Button"",
                    ""id"": ""71e4b152-66af-462a-8ada-1336229f2380"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""3975e712-dec0-4584-b85d-dfbecd5e9a18"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""QteB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ad820a58-5ce5-46ec-9ef4-d129c071333b"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""QteB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""45b28202-1935-4a7e-b6b1-68e184f91a33"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""QteA"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""211676c6-47ea-4a5a-aab6-663f77ccfeec"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""QteA"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""36cac9af-214e-4c5d-91ab-d283111f6d00"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""QteX"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""79794782-b42b-45e7-8f2b-0c7b3b5225a7"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""QteX"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e7fa7cb8-6db1-4835-a139-52d3d33b591e"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""QteY"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cd5f1b9c-c925-476b-9672-1e5eb8f76e2b"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""QteY"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7e75850d-3d69-43fa-98b5-2f77cc566a31"",
                    ""path"": ""<Keyboard>/f1"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""Layer1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a3ae8199-d02f-488c-80fd-88be6c35287b"",
                    ""path"": ""<Keyboard>/f2"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""Layer2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d4d280cc-e879-4bd2-851a-29de6a535319"",
                    ""path"": ""<Keyboard>/f3"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""Layer3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""KeyboardAndMouse"",
            ""bindingGroup"": ""KeyboardAndMouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_QteA = m_Player.FindAction("QteA", throwIfNotFound: true);
        m_Player_QteB = m_Player.FindAction("QteB", throwIfNotFound: true);
        m_Player_QteX = m_Player.FindAction("QteX", throwIfNotFound: true);
        m_Player_QteY = m_Player.FindAction("QteY", throwIfNotFound: true);
        m_Player_Layer1 = m_Player.FindAction("Layer1", throwIfNotFound: true);
        m_Player_Layer2 = m_Player.FindAction("Layer2", throwIfNotFound: true);
        m_Player_Layer3 = m_Player.FindAction("Layer3", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    public IEnumerable<InputBinding> bindings => asset.bindings;

    public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
    {
        return asset.FindAction(actionNameOrId, throwIfNotFound);
    }

    public int FindBinding(InputBinding bindingMask, out InputAction action)
    {
        return asset.FindBinding(bindingMask, out action);
    }

    // Player
    private readonly InputActionMap m_Player;
    private List<IPlayerActions> m_PlayerActionsCallbackInterfaces = new List<IPlayerActions>();
    private readonly InputAction m_Player_QteA;
    private readonly InputAction m_Player_QteB;
    private readonly InputAction m_Player_QteX;
    private readonly InputAction m_Player_QteY;
    private readonly InputAction m_Player_Layer1;
    private readonly InputAction m_Player_Layer2;
    private readonly InputAction m_Player_Layer3;
    public struct PlayerActions
    {
        private @PlayerInput m_Wrapper;
        public PlayerActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @QteA => m_Wrapper.m_Player_QteA;
        public InputAction @QteB => m_Wrapper.m_Player_QteB;
        public InputAction @QteX => m_Wrapper.m_Player_QteX;
        public InputAction @QteY => m_Wrapper.m_Player_QteY;
        public InputAction @Layer1 => m_Wrapper.m_Player_Layer1;
        public InputAction @Layer2 => m_Wrapper.m_Player_Layer2;
        public InputAction @Layer3 => m_Wrapper.m_Player_Layer3;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void AddCallbacks(IPlayerActions instance)
        {
            if (instance == null || m_Wrapper.m_PlayerActionsCallbackInterfaces.Contains(instance)) return;
            m_Wrapper.m_PlayerActionsCallbackInterfaces.Add(instance);
            @QteA.started += instance.OnQteA;
            @QteA.performed += instance.OnQteA;
            @QteA.canceled += instance.OnQteA;
            @QteB.started += instance.OnQteB;
            @QteB.performed += instance.OnQteB;
            @QteB.canceled += instance.OnQteB;
            @QteX.started += instance.OnQteX;
            @QteX.performed += instance.OnQteX;
            @QteX.canceled += instance.OnQteX;
            @QteY.started += instance.OnQteY;
            @QteY.performed += instance.OnQteY;
            @QteY.canceled += instance.OnQteY;
            @Layer1.started += instance.OnLayer1;
            @Layer1.performed += instance.OnLayer1;
            @Layer1.canceled += instance.OnLayer1;
            @Layer2.started += instance.OnLayer2;
            @Layer2.performed += instance.OnLayer2;
            @Layer2.canceled += instance.OnLayer2;
            @Layer3.started += instance.OnLayer3;
            @Layer3.performed += instance.OnLayer3;
            @Layer3.canceled += instance.OnLayer3;
        }

        private void UnregisterCallbacks(IPlayerActions instance)
        {
            @QteA.started -= instance.OnQteA;
            @QteA.performed -= instance.OnQteA;
            @QteA.canceled -= instance.OnQteA;
            @QteB.started -= instance.OnQteB;
            @QteB.performed -= instance.OnQteB;
            @QteB.canceled -= instance.OnQteB;
            @QteX.started -= instance.OnQteX;
            @QteX.performed -= instance.OnQteX;
            @QteX.canceled -= instance.OnQteX;
            @QteY.started -= instance.OnQteY;
            @QteY.performed -= instance.OnQteY;
            @QteY.canceled -= instance.OnQteY;
            @Layer1.started -= instance.OnLayer1;
            @Layer1.performed -= instance.OnLayer1;
            @Layer1.canceled -= instance.OnLayer1;
            @Layer2.started -= instance.OnLayer2;
            @Layer2.performed -= instance.OnLayer2;
            @Layer2.canceled -= instance.OnLayer2;
            @Layer3.started -= instance.OnLayer3;
            @Layer3.performed -= instance.OnLayer3;
            @Layer3.canceled -= instance.OnLayer3;
        }

        public void RemoveCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterfaces.Remove(instance))
                UnregisterCallbacks(instance);
        }

        public void SetCallbacks(IPlayerActions instance)
        {
            foreach (var item in m_Wrapper.m_PlayerActionsCallbackInterfaces)
                UnregisterCallbacks(item);
            m_Wrapper.m_PlayerActionsCallbackInterfaces.Clear();
            AddCallbacks(instance);
        }
    }
    public PlayerActions @Player => new PlayerActions(this);
    private int m_KeyboardAndMouseSchemeIndex = -1;
    public InputControlScheme KeyboardAndMouseScheme
    {
        get
        {
            if (m_KeyboardAndMouseSchemeIndex == -1) m_KeyboardAndMouseSchemeIndex = asset.FindControlSchemeIndex("KeyboardAndMouse");
            return asset.controlSchemes[m_KeyboardAndMouseSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface IPlayerActions
    {
        void OnQteA(InputAction.CallbackContext context);
        void OnQteB(InputAction.CallbackContext context);
        void OnQteX(InputAction.CallbackContext context);
        void OnQteY(InputAction.CallbackContext context);
        void OnLayer1(InputAction.CallbackContext context);
        void OnLayer2(InputAction.CallbackContext context);
        void OnLayer3(InputAction.CallbackContext context);
    }
}
