﻿using System;
using ShowIf;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;

namespace Qte
{
    [CreateAssetMenu(fileName = "QteCombo", menuName = "Data/QteCombo", order = 0)]
    public class QteCombo : ScriptableObject
    {
        public Action<bool> OnAvailabilityChanged;

        [SerializeField] private string name;
        [SerializeField] private QteAction[] combo;
        [SerializeField] private QteAffect affect;
        [SerializeField] private bool isAvailableByDefault;

        public string Name => name;

        public QteAction[] Combo => combo;

        public QteAffect Affect => affect;

        public bool IsAvailableByDefault => isAvailableByDefault;
        public bool IsEnabled => _isEnabled;

        private bool _isEnabled;

        public void SetAvailable(bool enable)
        {
            _isEnabled = enable;
            OnAvailabilityChanged?.Invoke(enable);
        }
    }


    [Serializable]
    public class QteAffect
    {
        [SerializeField] public QteAffectType affectType;

        [SerializeField] [Tooltip("Works only when type is SPAWN")]
        private UnitScript unitPrefab;

        [SerializeField] [Tooltip("Works only when type is UPGRADE")]
        private QteCombo comboToUnlock; 
        public QteAffectType AffectType => affectType;

        public UnitScript UnitPrefab => unitPrefab;

        public QteCombo ComboToUnlock => comboToUnlock;
    }
}