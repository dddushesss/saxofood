﻿using System;
using ShowIf;
using UnityEngine;

namespace Qte
{
    public enum QteAffectType
    {
        SPAWN,
        UPGRADE
    }
    [CreateAssetMenu(fileName = "QteSettings", menuName = "Data/Qte", order = 0)]
    public class QteData : ScriptableObject
    {
        public Action OnTick;
        public Action<UnitScript> OnSpawnAffect;

        [SerializeField] private float delta;
        [SerializeField] private int beatPerMinute = 120;

        public float Delta => delta;

        public int BeatPerMinute => beatPerMinute;
    }

    
}