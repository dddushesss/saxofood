﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Qte
{
    public enum QteAction
    {
        PAUSE,
        X,
        Y,
        A,
        B
    }

    [RequireComponent(typeof(AudioSource))]
    public class QteSystem : MonoBehaviour
    {
        public Action<QteCombo> OnCombo;
        public Action<QteAction> OnQteAction;
        public Action OnQteMiss;

        public QteCombo[] Combos => combos.ToArray();

        public List<QteAction> CurrentCombo => _currentCombo;

        [SerializeField] private QteData data;
        [SerializeField] private QteCombo[] combos;

        private PlayerInput _input;
        private float _elapsedTime;
        private float _lastTick;
        private float _elapsedTimeFromQteAction;
        private List<QteAction> _currentCombo = new();
        private AudioSource _source;

        private void Start()
        {
            foreach (var combo in combos)
            {
                SetComboAvailability(combo, combo.IsAvailableByDefault);
            }

            _source = GetComponent<AudioSource>();
            _input = new PlayerInput();
            _input.Enable();
            _input.Player.QteA.started += HandleOnAActionTriggered;
            _input.Player.QteB.started += HandleOnBActionTriggered;
            _input.Player.QteX.started += HandleOnXActionTriggered;
            _input.Player.QteY.started += HandleOnYActionTriggered;
            OnQteAction += HandleOnQteAction;
            OnQteMiss += HandleOnQteMiss;
            OnCombo += HandleOnCombo;
        }

        private void HandleOnCombo(QteCombo combo)
        {
            var affect = combo.Affect;
            switch (affect.AffectType)
            {
                case QteAffectType.SPAWN when affect.UnitPrefab != null:
                    data.OnSpawnAffect?.Invoke(affect.UnitPrefab);
                    break;
                case QteAffectType.UPGRADE when affect.ComboToUnlock != null:
                    SetComboAvailability(affect.ComboToUnlock, true);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _currentCombo.Clear();
        }

        private void SetComboAvailability(QteCombo combo, bool enable)
        {
            combo.SetAvailable(enable);
        }

        private void HandleOnQteMiss()
        {
            _currentCombo.Clear();
        }

        private void HandleOnQteAction(QteAction action)
        {
            _elapsedTimeFromQteAction = _elapsedTime;

            _currentCombo.Add(action);
            var isComboFound = false;

            foreach (var combo in combos.Where(c => c.IsEnabled))
            {
                var comboQueue = _currentCombo.ToList();
                var desiredCombo = combo.Combo.ToList();
                QteAction input;
                QteAction desiredInput;
                do
                {
                    input = comboQueue[0];
                    desiredInput = desiredCombo[0];
                    comboQueue.RemoveAt(0);
                    desiredCombo.RemoveAt(0);
                } while (comboQueue.Count > 0 && desiredCombo.Count > 0 && input == desiredInput);

                isComboFound = (comboQueue.Count == 0 && input == desiredInput) || isComboFound;
                if (desiredCombo.Count != 0 || input != desiredInput) continue;

                OnCombo?.Invoke(combo);
                break;
            }

            if (isComboFound) return;

            if (_currentCombo.Count == 1 && _currentCombo[0] == QteAction.PAUSE)
                _currentCombo.Clear();
            else
            {
                OnQteMiss?.Invoke();
            }
        }

        private bool IsInputOnThisTick()
        {
            var beatPerSec = GetBeatPerSec();
            return _elapsedTime - _elapsedTimeFromQteAction - 1 / (beatPerSec) < Time.deltaTime &&
                   _lastTick + 1 / (beatPerSec) - _elapsedTime > data.Delta + Time.deltaTime;
        }

        private void Update()
        {
            _elapsedTime += Time.deltaTime;
            var beatPerSec = GetBeatPerSec();
            if (_elapsedTime % (1 / beatPerSec) > data.Delta + Time.deltaTime && !IsInputOnThisTick() &&
                !IsInDeltaRange())
            {
                OnQteAction?.Invoke(QteAction.PAUSE);
            }

            if (!(_elapsedTime % (1 / beatPerSec) <= Time.deltaTime)) return;
            data.OnTick?.Invoke();
            _source.Play();
            _lastTick = _elapsedTime;
        }

        private void OnDestroy()
        {
            _input.Disable();
            _input.Player.QteA.started -= HandleOnAActionTriggered;
            _input.Player.QteB.started -= HandleOnBActionTriggered;
            _input.Player.QteX.started -= HandleOnXActionTriggered;
            _input.Player.QteY.started -= HandleOnYActionTriggered;
        }

        private bool IsInDeltaRange()
        {
            var beatPerSec = GetBeatPerSec();
            return _elapsedTime - _lastTick <= data.Delta + Time.deltaTime ||
                   _lastTick + 1 / (beatPerSec) - _elapsedTime <= data.Delta + Time.deltaTime;
        }

        private float GetBeatPerSec() => (float) (data.BeatPerMinute / 60.0);

        private void HandleOnAActionTriggered(InputAction.CallbackContext callbackContext)
        {
            if (IsMissedInput())
                return;

            OnQteAction?.Invoke(QteAction.A);
        }

        private void HandleOnBActionTriggered(InputAction.CallbackContext callbackContext)
        {
            if (IsMissedInput())
                return;

            OnQteAction?.Invoke(QteAction.B);
        }

        private void HandleOnXActionTriggered(InputAction.CallbackContext callbackContext)
        {
            if (IsMissedInput())
                return;

            OnQteAction?.Invoke(QteAction.X);
        }

        private void HandleOnYActionTriggered(InputAction.CallbackContext callbackContext)
        {
            if (IsMissedInput())
                return;

            OnQteAction?.Invoke(QteAction.Y);
        }

        private bool IsMissedInput()
        {
            if (IsInDeltaRange() && !IsInputOnThisTick()) return false;

            Debug.Log("Delta: " + IsInDeltaRange() + " inputOnThisTick: " + IsInputOnThisTick());
            OnQteMiss?.Invoke();
            return true;
        }
    }
}