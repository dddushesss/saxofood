﻿using System;
using System.Collections.Generic;
using Qte;
using UnityEngine;

namespace Sound
{
    [CreateAssetMenu(fileName = "QteSoundData", menuName = "Data/SoundData", order = 0)]
    public class QteSoundData : ScriptableObject
    {
        [SerializeField] private SoundComboType[] soundComboTypes;

        public SoundComboType[] SoundComboTypes => soundComboTypes;
    }

    [Serializable]
    public class SoundComboType
    {
        [SerializeField] private QteAction action;
        [SerializeField] private QteActionSound[] qteActionSound;

        public QteAction Action => action;

        public QteActionSound[] QteActionSound => qteActionSound;
    }

    [Serializable]
    public class QteActionSound
    {
        [SerializeField] private QteAction action;
        [SerializeField] private AudioClip[] audioClips;

        public QteAction Action => action;

        public AudioClip[] AudioClips => audioClips;
    }
}