﻿using System;
using System.Linq;
using Qte;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Sound
{
    [RequireComponent(typeof(AudioSource))]
    public class QteSoundSystem : MonoBehaviour
    {
        [SerializeField] private QteSystem qteSystem;
        [SerializeField] private QteSoundData data;

        private SoundComboType _currentType;
        private AudioSource _audioSource;
        private void Start()
        {
            qteSystem.OnQteAction += HandleOnQteAction;
            _audioSource = GetComponent<AudioSource>();
        }

        private void OnDestroy()
        {
            qteSystem.OnQteAction -= HandleOnQteAction;
        }

        private void HandleOnQteAction(QteAction action)
        {
            if (action == QteAction.PAUSE) return;

            if (qteSystem.CurrentCombo.Count == 1)
            {
                _currentType = data.SoundComboTypes.FirstOrDefault(type => type.Action == action);
            }

            var soundSet = _currentType?.QteActionSound.FirstOrDefault(actionSound => actionSound.Action == action);
            
            if (soundSet == null || soundSet.AudioClips.Length == 0) return;

            var audioClip = soundSet.AudioClips[Random.Range(0, soundSet.AudioClips.Length - 1)];
            
            _audioSource.clip = audioClip;
            _audioSource.Play();
        }
    }
}