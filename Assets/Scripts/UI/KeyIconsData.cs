﻿using System;
using Qte;
using UnityEngine;

namespace UI
{
    [CreateAssetMenu(fileName = "UiKeyData", menuName = "Data/UiData/KeyIcons", order = 0)]
    public class KeyIconsData : ScriptableObject
    {
        [SerializeField] private InputIcon[] icons;
        [SerializeField] private UiHintsView viewPrefab;

        public InputIcon[] Icons => icons;

        public UiHintsView ViewPrefab => viewPrefab;
    }

    [Serializable]
    public class InputIcon
    {
        [SerializeField] private Sprite icon;
        [SerializeField] private Sprite pressedIcon;
        [SerializeField] private QteAction action;

        public Sprite Icon => icon;

        public Sprite PressedIcon => pressedIcon;
        
        public QteAction Action => action;
    }
}