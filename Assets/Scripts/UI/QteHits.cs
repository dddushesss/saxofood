using System;
using System.Collections.Generic;
using System.Linq;
using Qte;
using UnityEngine;

namespace UI
{
    public class QteHits : MonoBehaviour
    {
        [SerializeField] private KeyIconsData data;
        [SerializeField] private QteSystem qteSystem;
        [SerializeField] private int hintsOnPage = 2;

        private List<List<UiHintsView>> _layers = new();
        private PlayerInput _input;

        private void Start()
        {
            var i = 0;
            var layerCount = 0;
            _input = new PlayerInput();
            _input.Enable();
            List<UiHintsView> currentLayer = null;
            
            foreach (var combo in qteSystem.Combos)
            {
                if (i == 0 || i % hintsOnPage == 0)
                {
                    layerCount++;
                    currentLayer = new List<UiHintsView>();
                    _layers.Add(currentLayer);
                }
                var hintView = Instantiate(data.ViewPrefab, transform);
                hintView.Init(data, combo);
                combo.OnAvailabilityChanged +=
                    (toActivate) => HandleOnQteComboAvailabilityChanged(hintView, toActivate);
                
                currentLayer?.Add(hintView);
                i++;
            }

            SetActiveLayer(0);
            _input.Player.Layer1.started += (_) => SetActiveLayer(0);
            _input.Player.Layer2.started += (_) => SetActiveLayer(1);
            _input.Player.Layer3.started += (_) => SetActiveLayer(2);

            qteSystem.OnQteAction += HandleOnQteAction;
            qteSystem.OnCombo += HandleOnCombo;
            qteSystem.OnQteMiss += HandleOnQteMiss;
        }

        private void OnDestroy()
        {
            _input.Disable();
            qteSystem.OnQteAction -= HandleOnQteAction;
            qteSystem.OnCombo -= HandleOnCombo;
            qteSystem.OnQteMiss -= HandleOnQteMiss;
        }

        private void HandleOnQteComboAvailabilityChanged(UiHintsView view, bool enable)
        {
            view.SetEnabled(enable);
        }
        private void SetActiveLayer(int index)
        {
            if (index >= _layers.Count) return;
            
            foreach (var view in _layers.SelectMany(layer => layer))
            {
                view.SetVisible(false);
            }

            foreach (var view in _layers[index])
            {
                view.SetVisible(true);
            }
        }

        private void HandleOnQteMiss()
        {
            foreach (var view in _layers.SelectMany(layer => layer))
            {
                view.ResetActive();
            }
        }

        private void HandleOnCombo(QteCombo combo)
        {
            foreach (var view in _layers.SelectMany(layer => layer))
            {
                view.ResetActive();
            }
        }

        private void HandleOnQteAction(QteAction action)
        {
            foreach (var view in _layers.SelectMany(layer => layer))
            {
                view.HandleOnQteAction(action);
            }
        }
    }
}