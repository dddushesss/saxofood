﻿using System.Collections.Generic;
using System.Linq;
using Qte;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UiHintsView : MonoBehaviour
    {
        [SerializeField] private Image iconPrefab;
        [SerializeField] private Transform iconParent;
        [SerializeField] private TMP_Text textField;

        private ComboView _comboViews;
        private KeyIconsData _data;
        private bool _isEnabled;
        private bool _isVisible;

        public void Init(KeyIconsData data, QteCombo combo)
        {
            var actionViews = new List<ComboActionView>();
            _data = data;
            foreach (var action in combo.Combo)
            {
                var iconSprite = data.Icons
                    .FirstOrDefault(icon => icon.Action == action);
                var icon = Instantiate(iconPrefab, iconParent);
                if (iconSprite == null) continue;

                icon.sprite = iconSprite.Icon;
                actionViews.Add(new ComboActionView(icon, iconSprite.PressedIcon, iconSprite.Icon, action));
            }

            _comboViews = new ComboView(actionViews.ToArray(), combo);
            textField.text = combo.Name;
        }

        public void SetVisible(bool isVisible)
        {
            _isVisible = isVisible;
            if (_isEnabled || !isVisible)
            {
                gameObject.SetActive(isVisible);
            }
        }

        public void SetEnabled(bool enable)
        {
            _isEnabled = enable;
            SetVisible(_isVisible);
        }

        public void HandleOnQteAction(QteAction action)
        {
            _comboViews.TryActivateNext(action);
        }

        public void ResetActive()
        {
            _comboViews.ResetActive();
        }
    }


    internal class ComboView
    {
        private readonly ComboActionView[] _actionViews;
        private readonly QteCombo _combo;

        public ComboView(ComboActionView[] actionViews, QteCombo combo)
        {
            _actionViews = actionViews;
            _combo = combo;
        }

        public void SetVisible(bool visible)
        {
            foreach (var view in _actionViews)
            {
                view._image.gameObject.SetActive(visible);
            }
        }

        public void TryActivateNext(QteAction action)
        {
            if (!_combo.IsEnabled)
            {
                return;
            }

            var nextInactiveActionView = _actionViews.FirstOrDefault((view) => !view.IsActive);

            if (nextInactiveActionView == null)
            {
                return;
            }

            if (nextInactiveActionView.Action != action)
            {
                ResetActive();
            }

            else
            {
                nextInactiveActionView.SetActive(true);
            }
        }

        public void ResetActive()
        {
            foreach (var view in _actionViews)
            {
                view.SetActive(false);
            }
        }
    }

    internal class ComboActionView
    {
        public readonly Image _image;
        private Sprite _activeIcon;
        private Sprite _inactiveIcon;
        private readonly QteAction _action;
        private bool _isActive = false;

        public bool IsActive => _isActive;

        public QteAction Action => _action;

        public ComboActionView(Image image, Sprite activeIcon, Sprite inactiveIcon, QteAction action)
        {
            _image = image;
            _activeIcon = activeIcon;
            _inactiveIcon = inactiveIcon;
            _action = action;
        }

        public void SetActive(bool activate)
        {
            _image.sprite = activate ? _activeIcon : _inactiveIcon;
            _isActive = activate;
        }
    }
}