using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class UnitScript : MonoBehaviour
{
    public Action OnDestroyRequest;
    public Action<int> OnDamagePlayer;

    [SerializeField] private bool isUpward = false;
    [SerializeField] private int greenHp = 1;
    [SerializeField] private int redHp = 1;
    [SerializeField] private Transform redHpContainer;
    [SerializeField] private Transform greenHpContainer;
    [SerializeField] private Image redHpImage;
    [SerializeField] private Image greenHpImage;

    private List<Image> redHpList = new();
    private List<Image> greenHpList = new();
    private GridCellScript cell;

    public bool IsUpward => isUpward;
    public int GreenHp => greenHp;
    public int RedHp => redHp;
    public void SetGreenHp(int greenHp) { this.greenHp = greenHp; }
    public void SetRedHp(int redHp) {  this.redHp = redHp; }

    public void SetGridCell(GridCellScript cell) { this.cell = cell; }

    public void FireOnDestroyRequest()
    {
        OnDestroyRequest?.Invoke();
    }

    public void UpdateHp()
    {
        var redHpToDelete = new List<Image>();
        var greenHpToDelete = new List<Image>();

        for (int i = Math.Abs(redHp); i < redHpList.Count; i++)
        {
            redHpList[i].DOFade(0f, 0.25f);
            redHpToDelete.Add(redHpList[i]);
        }

        foreach(Image image in redHpToDelete)
        {
            redHpList.Remove(image);
        }

        for (int i = Math.Abs(greenHp); i < greenHpList.Count; i++)
        {
            greenHpList[i].DOFade(0f, 0.25f);
            greenHpToDelete.Add(greenHpList[i]);
        }

        foreach (Image image in greenHpToDelete)
        {
            greenHpList.Remove(image);
        }

        redHpToDelete.Clear();
        greenHpToDelete.Clear();

    }

    public void MoveOnTick()
    {
        var nextCell = cell.GetNextCell(isUpward);
        if (nextCell != null)
        {
            transform.DOMove(nextCell.transform.position, 0.25f);
            this.cell = nextCell;
        } else
        {
            var dmg = isUpward ? 0 : greenHp + redHp;
            OnDamagePlayer?.Invoke(dmg);
            FireOnDestroyRequest();
        }
    }

    private void Start()
    {
        for (int i = 0; i < Math.Abs(RedHp); i++)
        {
            redHpList.Add(Instantiate(redHpImage, redHpContainer));
        }

        for(int i = 0;i < Math.Abs(greenHp); i++)
        {
            greenHpList.Add(Instantiate(greenHpImage, greenHpContainer));
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<UnitScript>(out var otherUnit))
        {
            if (otherUnit.isUpward != this.isUpward && !this.isUpward)
            {
                var greenHpTotal = this.greenHp + otherUnit.greenHp;
                var redHpTotal = this.redHp + otherUnit.redHp;

                if (redHpTotal < 0 && greenHpTotal < 0)
                {
                    otherUnit.greenHp = greenHpTotal;
                    otherUnit.redHp = redHpTotal;
                    otherUnit.UpdateHp();
                    FireOnDestroyRequest();
                }

                if (redHpTotal > 0 || greenHpTotal > 0) 
                {
                    this.greenHp = greenHpTotal;
                    this.redHp = redHpTotal;
                    UpdateHp();
                    otherUnit.FireOnDestroyRequest();
                }

                if (redHpTotal == 0 && greenHpTotal == 0)
                {
                    FireOnDestroyRequest();
                    otherUnit.FireOnDestroyRequest();
                }
            }
        }
    }




}
