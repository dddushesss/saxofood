using DG.Tweening;
using Qte;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UnitSpawnerScript : MonoBehaviour
{

    [SerializeField] private GridCellScript spawnCell;
    [SerializeField] private int moveRateInTicks = 2;
    [SerializeField] private QteData data;
    [SerializeField] private SpawnTickInfo[] spawnTimings;
    [SerializeField] private int tickOffset = 0;
    [SerializeField] private bool spawnOnQte = false;
    [SerializeField] private HpBarScript playerHp;

    private int _ticksCount = 0;
    private readonly List<UnitScript> _scriptList = new();
    private readonly List<UnitScript> _listToDelete = new();

    private readonly Dictionary<int, UnitScript> _scriptDic = new();

    // Start is called before the first frame update
    void Start()
    {
        _ticksCount += tickOffset;
        data.OnTick += HandleOnTick;
        if (spawnOnQte )
        {
            data.OnSpawnAffect += SpawnUnit;
        }

        foreach (var spawntime in spawnTimings)
        {
            _scriptDic.Add(spawntime.spawnTimeInTicks, spawntime.unitScript);
        }
    }

    private void HandleOnTick()
    {
        _ticksCount++;
        if (_scriptDic.TryGetValue(_ticksCount, out var newUnit))
        {
            SpawnUnit(newUnit);
        }

        if (_ticksCount % moveRateInTicks != 0 || moveRateInTicks <= 0) return;
        
        foreach (var unit in _scriptList.Where(unit => unit != null))
        {
            unit.MoveOnTick();
        }
        
        foreach (var unit in _listToDelete)
        {
            _scriptList.Remove(unit);
        }
        _listToDelete.Clear();
    }

    private void SpawnUnit(UnitScript newUnit)
    {
        var unit = Instantiate(newUnit, spawnCell.transform.position, spawnCell.transform.rotation);
        unit.SetGridCell(spawnCell);
        _scriptList.Add(unit);
        unit.OnDestroyRequest += () => HandleDestroyRequest(unit);
        if (!spawnOnQte)
        {
            unit.OnDamagePlayer += HandlePlayerDamage;
        }
    }

    private void HandlePlayerDamage(int dmg)
    {
        playerHp.DealDamage(dmg);
    }

    private void HandleDestroyRequest(UnitScript unit)
    {
        _listToDelete.Add(unit);
        unit.transform.DOKill();
        Destroy(unit.gameObject);
    }
}


[Serializable]
public class SpawnTickInfo
{
    [SerializeField] public int spawnTimeInTicks;
    [SerializeField] public UnitScript unitScript;
}
